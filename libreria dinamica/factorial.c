#include "libstop.h"
#include <stdio.h>

int main() 
/* Programa de Factorial de un numero*/
{
     printf ("Programa de Factorial de un numero\n");
    int n;
    int f;
    /*Solicitando el numero para calcular el Factorial de un numero*/
    printf("Ingrese el número para calcular el factorial:\t \n");
    scanf("%d",&n);
    
    if (n<0){
        printf("Error ingrese un valor correcto");
    
    }else{
        f=factorial(n);
        /* Resultado del Factorial de un numero*/
        printf("El factorial del numero %d!=%d\n",n,f);
    }
return 0;
}

long factorial (int n){
    if(n==0 || n==1){
        return 1;
    }else if(n>2) {
        int factorial=1;
        while (n>1)
        {
         factorial*=n; 
         n --;  
        }
        return factorial;

        }   
    }


